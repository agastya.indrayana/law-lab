from django.shortcuts import render
from django.views.generic import TemplateView 

import json
import requests

class Home(TemplateView):
    template_name = "index.html"

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context=context)

